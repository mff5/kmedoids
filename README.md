# Final k-medoids

Jedná sa o finálne riešnie.

## Zrýchlenie

Zrýchlenie je len 5.4-násobné oproti sériovej verzii púšťanej na parlabe. (PARLAB, sériové: 255.695; GPULAB, paralelné: 44.4542)

Testované na `aloi_crop.bsf`. Plný aloi som vzhľadom k malému zrýchleniu ani nepúšťal.

## Princíp

CUDA sa používa na počítanie SQFD vzdialenosti medzi jednotlivými objektami. Všetky objekty sú na začiatku odoslané na GPU a následne sa pre výpočet vzdialenosti posielajú už len ich indexy. Vzdialenosť sa počíta vo väčších množstvách a tak ku kopírovaniu nedochádz pri každom volaní funkie na výpočet vzdialenosti SQFD ale len vtedy, kedy je to potrebné. 

Inak sa dáta delia medzi jednotlivé MPI procesy. Každý MPI proces spracúva nejakú časť dát, aby bolo spracovanie ideálne paralelné. Následne sú dáta zlivané dohromady.

## Kompilácia a spustenie

Na kompiláciu je možné použiť skript `make.sh` v adresári `parallel`, ktorý paralelné riešenie skompiluje. Na spustenie je následne možné použiť skript `run.sh` v rovnakom adresári, ten to spustí s potrebnými argumentami. Samotný skript potrebuje jeden argument a to vstupný súbor ".bsf".
