#!/bin/bash

# srun -A nprg058s -p gpu-short --gres=gpu:volta:1 -n 4 -N 4 ./parallel --iterations 8 --images 64 --k 16 $1
srun -A nprg058s -p gpu-short --gres=gpu:volta:1 -n 4 -N 4 ./parallel $1
