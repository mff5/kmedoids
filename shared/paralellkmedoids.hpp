#ifndef PARALLEL_KMEDOIDS_SHARED_KMEDOIDS_HPP
#define PARALLEL_KMEDOIDS_SHARED_KMEDOIDS_HPP

#include "misc/exception.hpp"

#include <mpi.h>

#include <vector>
#include <algorithm>
#include <random>
#include <cstdio>

#include "macros.hpp"

struct Partitions {
	std::size_t objectPartition;
	std::size_t objectPartitionOffset;

	std::size_t assignmentPartiton;
	std::size_t assignmentPartitonOffset;

	std::size_t medoidPartition;
	std::size_t medoidPartitionOffset;

	std::size_t clusterPartition; //how many cluster I have
	std::size_t clusterPartitionOffset; //which cluster is the first (of all clusters)

	void print() {
		RANKS

		std::cout << "Rank: " << rank << std::endl;
		std::cout << " " << rank << "  objectPartition: " << objectPartition << std::endl;
		std::cout << " " << rank << "  objectPartitionOffset: " << objectPartitionOffset << std::endl;
		std::cout << " " << rank << "  ..." << std::endl;
		std::cout << " " << rank << "  assignmentPartiton: " << assignmentPartiton << std::endl;
		std::cout << " " << rank << "  assignmentPartitonOffset: " << assignmentPartitonOffset << std::endl;
		std::cout << " " << rank << "  ..." << std::endl;
		std::cout << " " << rank << "  medoidPartition: " << medoidPartition << std::endl;
		std::cout << " " << rank << "  medoidPartitionOffset: " << medoidPartitionOffset << std::endl;
		std::cout << " " << rank << "  ..." << std::endl;
		std::cout << " " << rank << "  clusterPartition: " << medoidPartition << std::endl;
		std::cout << " " << rank << "  clusterPartitionOffset: " << medoidPartitionOffset << std::endl;
		std::cout << " " << rank << "  ---" << std::endl;
		std::cout << " " << rank << "  ---" << std::endl;
	}
};

/**
 * \brief A K-medoids algorithm.
 * \tparam OBJ_CONTAINER Container holding all database objects. Must implement [] and size() like std::vector.
 * \tparam DIST Type of distance functor that computes/provides distances between objects.
 *		The functor must have operator() that takes two OBJs (items from OBJ_CONTAINER) and yields a FLOAT.
 * \tparam FLOAT Float data type with selected precision (used for distances and medoid scores).
 *		It should be set to float or double.
 */
template<class OBJ_CONTAINER, class DIST, typename FLOAT = float>
class ParallelKMedoids
{
protected:
	DIST& mDistFnc;					///< Functor used to compute object distances (OBJ, OBJ) to FLOAT
	std::size_t mK;					///< Number of desired clusters
	std::size_t mMaxIters;			///< Maximal number of algorithm iterations.

	// Distance statistics
	FLOAT mLastAvgDistance;				///< Average distance between an object and its respective medoid (computed with last update).
	FLOAT mLastAvgClusterDistance;		///< Average distance of average distances within each cluster (computed with last update).

	std::size_t centroids;

	void printResultStats(std::size_t iter, std::vector<std::size_t>& medoids, std::vector<std::size_t>& assignments)
	{
		RANKS

		std::vector<std::size_t> sizes(medoids.size());
		for (auto&& a : assignments) {
			++sizes[a];
		}

		std::cout << "Rank: " << rank << ", " << iter << ": ";

		for (std::size_t i = 0; i < medoids.size(); ++i) {
			std::cout << medoids[i] << " (" << sizes[i] << "), ";
		}
		std::cout << std::endl;
	}

	/**
	 * \brief Verify algorithm parameters. If they do conflict, LogicError is thrown.
	 */
	virtual void checkParams(std::size_t objectsCount)
	{
		if (mK < 2)
			throw (bpp::LogicError() << "Too few clusters (" << mK << ") selected. The k value must be at least 2.");
		if (mMaxIters == 0)
			throw (bpp::LogicError() << "At least one iteration must be allowed in the algorithm.");
		if (mK > objectsCount)
			throw (bpp::LogicError() << "The algorithm is requested to create " << mK << " clusters of " << objectsCount << " objects.");
	}


	/**
	 * \brief Create the initial selection of medoids (uniform random), unless they are already set.
	 */
	void initRandMedoids(std::vector<std::size_t>& medoids, std::size_t objectCount) const
	{
		if (medoids.size() == mK) {
			// medoids are already set, just check them out
			std::sort(medoids.begin(), medoids.end());
			for (std::size_t i = 0; i < mK; ++i) {
				if (medoids[i] >= objectCount) {
					throw (bpp::RuntimeError() << "Initial medoid indices are out of range.");
				}
				if (i > 0 && medoids[i] == medoids[i - 1]) {
					throw (bpp::RuntimeError() << "Initial medoid indices contain duplicates.");
				}
			}
		}
		else {
			// initialize mediods with random seletion
			medoids.resize(objectCount);
			for (std::size_t i = 0; i < objectCount; ++i) {
				medoids[i] = i;
			}

			std::random_device rd;
			std::mt19937 gen(rd());
			std::shuffle(medoids.begin(), medoids.end(), gen);
		}
	}


	/**
	 * \brief Compute current assignment of objects to medoids according to distance functor.
	 * \param objects Input set of objects to be clustered.
	 * \param medoids The medoid objects (as indices to the objects vector).
	 * \param assignments The result assignment value for each object. The assignment vector
	 *		has the same size as objects vector and each value is an index to medoids vector.
	 * \param distances Pointer to an array, where distances to assigned medoids are kept.
	 *		If nullptr, the distances are not saved.
	 */
	void computeAssignments(std::size_t objectCount, std::size_t objectOffset, std::vector<std::size_t>& medoids, std::vector<std::size_t>& assignments, FLOAT* distances = nullptr) const {
		RANKS

		auto normalize = [objectOffset] (std::size_t i) {
			return i + objectOffset;
		};

		mDistFnc.sqfd_prepare_output_medoid(objectCount, medoids.size());


		for (std::size_t i = 0; i < objectCount; i++) {
			for (std::size_t m = 0; m < medoids.size(); ++m) {
				mDistFnc(normalize(i), medoids[m]);
			}
		}

		// mDistFnc(objectCount, medoids.size());

		std::vector<float> rv = mDistFnc.sqfd_collect_results(objectCount, medoids.size(), true);

		for (std::size_t i = 0; i < objectCount; ++i) {
			auto elem = std::min_element(rv.begin() + i * medoids.size(), rv.begin() + (i + 1) * medoids.size());
			assignments[i] = elem - (rv.begin() + i * medoids.size());
		}
	}

	/**
	 * \brief Compute the score of selected medoid within given cluster as a sum of distance squares.
	 * \param objects Input set of objects to be clustered.
	 * \param cluster List of object indices within the examined cluster.
	 * \param medoid Index of selected medoid in cluster vector.
	 * \return The score of the selected medoid.
	 */
	FLOAT computeMedoidScore(const std::vector<std::size_t>& cluster, std::size_t medoid) const
	{
		mDistFnc.sqfd_prepare_output_medoid(cluster.size() - 1, 1);
		for (std::size_t i = 0; i < cluster.size(); ++i) {
			if (i == medoid) continue;
			mDistFnc(cluster[medoid], cluster[i]);
		}

		std::vector<float> rv = mDistFnc.sqfd_collect_results(cluster.size() - 1, 1, false);
		return std::accumulate(rv.begin(), rv.end(), 0.0);
	}


	/**
	 * \brief Find the best medoid for selected cluster and return its object index.
	 * \param objects Input set of objects to be clustered.
	 * \param cluster List of object indices within the examined cluster.
	 * \param bestScore Output value where best distance sum of returned medoid is stored.
	 * \return Index of the best medoid found (in the objects collection).
	 */
	std::size_t getBestMedoid(const std::vector<std::size_t>& cluster, FLOAT& bestScore) const
	{
		// The cluster is never empty!
		if (cluster.empty()) {
			throw (bpp::RuntimeError() << "Unable to select the best medoid of an empty cluster.");
		}

		// One-medoid show and zwei-medoid buntes are easy to solve.
		if (cluster.size() < 3) {
			return cluster[0];
		}

		// Find a medoid with smallest score.
		std::size_t medoid = 0;
		bestScore = computeMedoidScore(cluster, medoid);

		for (std::size_t i = 1; i < cluster.size(); ++i) {
			FLOAT score = computeMedoidScore(cluster, i);
			if (score < bestScore) {
				bestScore = score;
				medoid = i;
			}
		}

		return cluster[medoid];
	}

	void create_partitions(std::vector<int>& partitions, std::vector<int>& offsets, int count) {
		RANKS

		int smallV = (int)std::floor( ( (float) count) / (float) ranks);
		int bigC = count - ranks * smallV;

		partitions.resize(ranks, smallV);
		offsets.resize(ranks, 0);

		for (int i = 0; i < bigC; i++) {
			if (i == 0) {
				offsets[i] = 0;
			}
			else {
				offsets[i] = offsets[i - 1] + smallV + 1;
			}
			partitions[i]++;
		}

		for (int i = bigC; i < ranks; i++) {
			if (i == 0) {
				offsets[i] = 0;
			}
			else {
				offsets[i] = offsets[i - 1] + smallV;
			}
		}
	}

	/**
	 * \brief Update the medoids according to the new assignments.
	 * \param objects Input set of objects to be clustered.
	 * \param medoids The medoid objects (as indices to the objects vector).
	 * \param assignments The assignment value for each object. The assignment vector
	 *		has the same size as objects vector and each value is an index to medoids vector.
	 * \return True if the medoids vector has been modified, false otherwise.
	 *		If the medoids have not been modified, the algorithm has reached a stable state.
	 */
	bool updateMedoids(Partitions part, std::vector<std::size_t>& medoids, const std::vector<std::size_t>& assignments, std::vector<FLOAT> mBestScores)
	{
		RANKS

		// Construct a cluster index (vector of clusters, each cluster is a vector of object indices).
		std::vector< std::vector< std::size_t>> clusters;
		clusters.resize(medoids.size()); // one cluster per medoid

		// Spread objects into clusters based on the assignments vector.
		for (std::size_t i = 0; i < assignments.size(); ++i) {
			if (part.clusterPartitionOffset <= assignments[i] && assignments[i] < part.clusterPartitionOffset + part.clusterPartition) {
				clusters[assignments[i] - part.medoidPartitionOffset].push_back(i);
			}
		}

		// Compute changes (new scores and medoids).
		mLastAvgDistance = mLastAvgClusterDistance = (FLOAT)0.0;

		std::size_t clusterCount = clusters.size();
		mBestScores.resize(clusterCount);
		std::vector<std::size_t> newMedoids(clusterCount);

		// Find the best medoid for each cluster.
		for (std::size_t m = 0; m < clusterCount; ++m) {
			mBestScores[m] = (FLOAT)0.0;
			newMedoids[m] = getBestMedoid(clusters[m], mBestScores[m]);
		}

		bool changed = false;	// whether the medoids vector was modified
		for (std::size_t m = 0; m < clusters.size(); ++m) {
			mLastAvgDistance += mBestScores[m];
			if (clusters[m].size() > 1) {
				mLastAvgClusterDistance += mBestScores[m] / (FLOAT)(clusters[m].size() - 1);
			}

			changed = changed || (newMedoids[m] != medoids[m]);
			medoids[m] = newMedoids[m];
		}

		mLastAvgDistance /= (FLOAT)(assignments.size() - clusters.size());	// all objects except medoids
		mLastAvgClusterDistance /= (FLOAT)clusters.size();					// all clusters

		// Report whether the medoids vector has been modified (if not -> early termination).
		return changed;
	}


	/**
	 * \brief Internal run method called by public run() interface.
	 * \param objects Input set of objects to be clustered.
	 * \param medoids The result medoid objects (as indices to the objects vector).
	 * \param assignments The result assignment value for each object. The assignment vector
	 *		has the same size as objects vector and each value is an index to medoids vector.
	 * \param distances Pointer to an array, where distances to assigned medoids are kept.
	 *		If nullptr, the distances are not saved.
	 * \return Number of iterations performed.
	 */
	virtual std::size_t runPrivate(const OBJ_CONTAINER& objects, std::vector<std::size_t>& medoids,
		std::vector<std::size_t>& assignments, FLOAT* distances = nullptr)
	{
		RANKS

		std::vector<int> objectPartition;
		std::vector<int> objectPartitionOffsets;
		// std::vector<int>& assignmentPartiton = objectPartition;
		// std::vector<int>& assignmentPartitonOffsets = objectPartitionOffsets;

		std::vector<int> medoidPartition;
		std::vector<int> medoidPartitionOffsets;
		// std::vector<int>& clusterPartition = medoidPartition;
		// std::vector<int>& clusterPartitionOffsets = medoidPartitionOffsets;

		std::size_t objectCount = objects.size();
		create_partitions(objectPartition, objectPartitionOffsets, objectCount);
		create_partitions(medoidPartition, medoidPartitionOffsets, mK);

		Partitions p;
		std::vector<FLOAT> bestScores;
		bestScores.resize(mK);

		p.assignmentPartiton = objectPartition[rank];
		p.assignmentPartitonOffset = objectPartitionOffsets[rank];

		p.objectPartition = objectPartition[rank];
		p.objectPartitionOffset = objectPartitionOffsets[rank];

		p.clusterPartition = medoidPartition[rank];
		p.clusterPartitionOffset = medoidPartitionOffsets[rank];

		p.medoidPartition = medoidPartition[rank];
		p.medoidPartitionOffset = medoidPartitionOffsets[rank];

		// p.print();

		if (rank == 0) {
			checkParams(objectCount);
			initRandMedoids(medoids, objectCount);
		}

		medoids.resize(mK);

		if (rank == 0) {
			std::cout << "\nMedoids:\n[ ";
			for (std::size_t i = 0; i < mK; i++) {
				std::cout << medoids[i] << " ";
			}
			std::cout << "]\n";
		}

		MPI_Bcast(medoids.data(), mK, MPI_UNSIGNED_LONG_LONG, 0, MPI_COMM_WORLD);

		std::vector<std::size_t> localMedoids(p.medoidPartition);
		std::vector<std::size_t> localAssignments(p.assignmentPartiton);

		localAssignments.resize(p.objectPartition);
		assignments.resize(objectCount);

		std::size_t iter = 0;
		while (iter < mMaxIters) {
			// Compute new assignments of objects to medoids.
			computeAssignments(p.objectPartition, p.objectPartitionOffset, medoids, localAssignments, nullptr);

			MPI_Allgatherv(localAssignments.data(), localAssignments.size(), MPI_UNSIGNED_LONG_LONG,
						   assignments.data(), objectPartition.data(), objectPartitionOffsets.data(), 
						   MPI_UNSIGNED_LONG_LONG, MPI_COMM_WORLD);

			MPI_Scatterv(medoids.data(), medoidPartition.data(), medoidPartitionOffsets.data(), MPI_UNSIGNED_LONG_LONG,
						 localMedoids.data(), medoidPartition[rank], MPI_UNSIGNED_LONG_LONG,
						 0, MPI_COMM_WORLD);

			
			// Update medoids (terminate if no update occured).
			bool updatedMedoids = updateMedoids(p, localMedoids, assignments, bestScores);

			MPI_Allreduce(&updatedMedoids, &updatedMedoids, 1, MPI_C_BOOL, MPI_LOR, MPI_COMM_WORLD);

			MPI_Allgatherv(localMedoids.data(), localMedoids.size(), MPI_UNSIGNED_LONG_LONG,
						   medoids.data(), medoidPartition.data(), medoidPartitionOffsets.data(), 
						   MPI_UNSIGNED_LONG_LONG, MPI_COMM_WORLD);

			if (!updatedMedoids) {
				break;
			}
			++iter;
		}

		return iter;
	}

public:
	ParallelKMedoids(DIST& distFnc, std::size_t k, std::size_t maxIters, std::size_t c)
		: mDistFnc(distFnc), mK(k), mMaxIters(maxIters), mLastAvgDistance(0), mLastAvgClusterDistance(0), centroids(c) {}

	std::size_t getK() const { return mK; }
	void setK(std::size_t k) { mK = k; }

	std::size_t getMaxIters() const { return mMaxIters; }
	void setMaxIters(std::size_t maxIters) { mMaxIters = maxIters; }

	FLOAT getLastAvgDistance() const { return mLastAvgDistance; }
	FLOAT getLastAvgClusterDistance() const { return mLastAvgClusterDistance; }

	/**
	* \brief Run the k-medoids clustering on given data.
	* \param objects Pointer to a C array of objects.
	* \param objectCount Number of objects in the input array.
	* \param medoids The result medoid objects (as indices to the objects vector).
	* \param assignments The result assignment value for each object. The assignment vector
	*		has the same size as objects vector and each value is an index to medoids vector.
	 * \param distances Pointer to an array, where distances to assigned medoids are kept.
	 *		If nullptr, the distances are not saved.
	* \return Number of iterations performed.
	*/
	std::size_t run(const OBJ_CONTAINER& objects, std::vector<std::size_t>& medoids,
		std::vector<std::size_t>& assignments, FLOAT* distances = nullptr)
	{
		// Just recall private virtual method...
		return this->runPrivate(objects, medoids, assignments, distances);
	}
};

#endif //PARALLEL_KMEDOIDS_SHARED_KMEDOIDS_HPP
