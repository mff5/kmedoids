#include "kernels.cuh"

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

__device__ float distance(const float* first, const float* second) {

    float dist = (float)0.0;
    float diff = 0.0;
    for (std::size_t d = 0; d < 7; ++d) {
        diff = first[d] - second[d];
        dist += diff * diff;
    }
    return dist;
}

__device__ float similarity(const float* first, const float* second, float alpha) {
    return exp(-alpha * distance(first, second));
}

__device__ void sqfd_distance(const float* centroids, const float* centroidWeights, const std::size_t* centroidCounts, 
                              const std::size_t* centroidOffsets, std::size_t objectCount,
                              std::size_t idx1, std::size_t idx2, float alpha, float* outVal) 
{
    std::size_t idxI = blockIdx.x * blockDim.x + threadIdx.x;
    std::size_t idxJ = blockIdx.y * blockDim.y + threadIdx.y;

    float res = 0.0;

    std::size_t centroid_offset_1 = centroidOffsets[idx1];
    std::size_t centroid_offset_2 = centroidOffsets[idx2];

    const float* first = centroids + (centroid_offset_1) * 7;
    const float* second = centroids + (centroid_offset_2) * 7;
    const float* firstWeights = centroidWeights + centroidOffsets[idx1];
    const float* secondWeights = centroidWeights + centroidOffsets[idx2];

    std::size_t count1 = centroidCounts[idx1];
    std::size_t count2 = centroidCounts[idx2];

    std::size_t idnI = idxI;
    std::size_t idnJ = idxJ;

    if (idnI < count1 && idnJ < count1) {
        res = firstWeights[idnI] * similarity(first + idnI * 7, first + idnJ * 7, alpha) * firstWeights[idnJ];
    }
    else if (idnI < count1 && idnJ >= count1 && idnJ < count1 + count2) {
        res = firstWeights[idnI] * similarity(first + idnI * 7, second + (idnJ - count1) * 7, alpha) * -secondWeights[idnJ - count1];
    }
    else if (idnI >= count1 && idnI < count1 + count2 && idnJ < count1) {
        res = -secondWeights[idnI - count1] * similarity(second + (idnI - count1) * 7, first + idnJ * 7, alpha) * firstWeights[idnJ];
    }
    else if (idnI >= count1 && idnI < count1 + count2 && idnJ >= count1 && idnJ < count1 + count2) {
        res = -secondWeights[idnI - count1] * similarity(second + (idnI - count1) * 7, second + (idnJ - count1) * 7, alpha) * -secondWeights[idnJ - count1];
    }
    else {
        return;
    }

    int lane_id = threadIdx.x & 0x1f;

    for (int i = 16; i>=1; i/=2) {
        res += __shfl_xor_sync(0xffffffff, res, i, 32);
    }
    
    if (lane_id == 0)
        atomicAdd(outVal, res);
}

__global__ void sqfd_distance_2(const float* centroids, const float* centroidWeights, const std::size_t* centroidCounts, 
                              const std::size_t* centroidOffsets, std::size_t objectCount,
                              float alpha, float* outVal) 
{
    std::size_t idx1 = blockIdx.x * blockDim.x + threadIdx.x;
    std::size_t idx2 = blockIdx.y * blockDim.y + threadIdx.y;

    float res = 0.0;

    std::size_t centroid_offset_1 = centroidOffsets[idx1];
    std::size_t centroid_offset_2 = centroidOffsets[idx2];

    const float* first = centroids + (centroid_offset_1) * 7;
    const float* second = centroids + (centroid_offset_2) * 7;
    const float* firstWeights = centroidWeights + centroidOffsets[idx1];
    const float* secondWeights = centroidWeights + centroidOffsets[idx2];

    std::size_t count1 = centroidCounts[idx1];
    std::size_t count2 = centroidCounts[idx2];

    for (std::size_t i = 0; i < count1; i++) {
        for (std::size_t j = 0; j < count1; i++) {
            res += firstWeights[i] * similarity(first + i * 7, first + j * 7, alpha) * firstWeights[j];
        }
    }

    for (std::size_t i = 0; i < count1; i++) {
        for (std::size_t j = 0; j < count2; i++) {
            res += 2 * firstWeights[i] * similarity(first + i * 7, second + j * 7, alpha) * -secondWeights[j];
        }
    }

    for (std::size_t i = 0; i < count2; i++) {
        for (std::size_t j = 0; j < count2; i++) {
            res += -secondWeights[i] * similarity(second + i * 7, second + j * 7, alpha) * -secondWeights[j];
        }
    }

    outVal[idx1 + idx2 * count1] = res;
}

__global__ void global_sqfd_distance(const float* centroids, const float* centroidWeights, const size_t* centroidCounts, const std::size_t* centroidOffsets,
                                     std::size_t objectCount, std::size_t idx1, std::size_t idx2, float alpha, float* outVal)
{
    sqfd_distance(centroids, centroidWeights, centroidCounts, centroidOffsets, objectCount, idx1, idx2, alpha, outVal);       
}

__global__ void print_contents(const float* centroids, const float* centroidWeights, const std::size_t* centroidCounts, const std::size_t* centroidOffsets,
                               std::size_t objectCount)
{
    printf("objectCount: %llu\n", objectCount);
    for (std::size_t i = 0; i < objectCount; i++) {
        printf("  centroidCounts[%llu]: %llu\n", i, centroidCounts[i]);
        printf("  centroidWeights[%llu]: %f\n", i, centroidWeights[i]);
        printf("  centroidOffsets[%llu]: %llu\n", i, centroidOffsets[i]);
        printf("\n");
    }
    printf("END PRINT\n");
}

// CALLER(S)

void run_sqfd_distance(const float* centroids, const float* centroidWeights, const std::size_t* centroidCounts,
                       const std::size_t* centroidOffsets, std::size_t objectCount, std::size_t idx1, std::size_t idx2,
                       float alpha, float* outVal, std::size_t count1, std::size_t count2)
{

    dim3 grid((count1 + count2) / 16 + 1, (count1 + count2) / 16 + 1);
    dim3 block(16, 16);
    global_sqfd_distance<<<grid, block>>>(centroids, centroidWeights, centroidCounts, centroidOffsets, objectCount, idx1, idx2, alpha, outVal);
}

void run_sqfd_distance_2(const float* centroids, const float* centroidWeights, const std::size_t* centroidCounts,
                         const std::size_t* centroidOffsets, std::size_t objectCount,
                         float alpha, float* outVal, std::size_t count1, std::size_t count2)
{
    dim3 grid(count1 / 16 + 1, count2 / 16 + 1);
    dim3 block(16, 16);
    sqfd_distance_2<<<grid, block>>>(centroids, centroidWeights, centroidCounts, centroidOffsets, objectCount, alpha, outVal);
}

void run_print_contents(const float* centroids, const float* centroidWeights, const std::size_t* centroidCounts, const std::size_t* centroidOffsets,
                        std::size_t objectCount)
{
    print_contents<<<1,1>>>(centroids, centroidWeights, centroidCounts, centroidOffsets, objectCount);
}
