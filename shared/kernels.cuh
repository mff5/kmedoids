#ifndef KERNELS_KMETOIDS
#define KERNELS_KMETOIDS

#include "cuda/cuda.hpp"
#include <iostream>

// template<int DIM, typename NUM_TYPE, typename IN_TYPE, int LP>
void run_sqfd_distance(const float* objects, const float* objectWeights, const std::size_t* objectCounts,
                       const std::size_t* centroidOffsets, std::size_t objectCount, std::size_t idx1, std::size_t idx2,
                       float alpha, float* outVal, std::size_t count1, std::size_t count2);

void run_print_contents(const float* objects, const float* objectWeights, const std::size_t* objectCounts, const std::size_t* centroidOffsets,
                        std::size_t objectCount);

#endif //KERNELS_KMETOIDS
