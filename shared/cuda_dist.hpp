#ifndef CUDA_DIST_HPP
#define CUDA_DIST_HPP

#include "kernels.cuh"
#include "sqfd.hpp"

#include <vector>
#include <array>
#include <cstdio>

template<int DIM, typename NUM_TYPE = float, typename IN_TYPE = float, int LP = 2000>
struct CudaDist {

    std::size_t centroids;

    CudaDist(IN_TYPE alpha, const DBSignatureListMapped<DIM, NUM_TYPE>& s, std::size_t c)
        : source(s)
    {
        mAlpha = alpha;
        cuda_init();
        objectCount = source.size();
        centroids = c;
        cuda_alloc();
        cuResultSize = 0;
        
        load_objects();
        std::cout << "Objects loaded into CUDA" << std::endl;
    }

    void operator() (std::size_t index1, std::size_t index2) {
        run_sqfd_distance(cuObjectsPtr, cuObjectWeightsPtr, cuObjectCentroidCount, cuCentroidOffsets, objectCount, index1, index2, mAlpha, 
            cuResultMoving, source[index1].getCentroidCount(), source[index2].getCentroidCount());

        cuResultMoving++;

        cudaDeviceSynchronize();
    }

    void sqfd_prepare_output_medoid(const std::size_t first_range_size, const std::size_t medoid_count)
    {
        if (cuResultSize < first_range_size * medoid_count) {
            CUCH(cudaFree(cuResult));
            CUCH(cudaMalloc((void**)&cuResult, sizeof(float) * first_range_size * medoid_count));
        }
        CUCH(cudaMemset(cuResult, 0, sizeof(float) * first_range_size * medoid_count));
        cuResultMoving = cuResult;
    }

    std::vector<NUM_TYPE> sqfd_collect_results(const std::size_t first_range_size, const std::size_t medoid_count, bool sqrt) {
        cudaDeviceSynchronize();
        std::vector<float> result;
        result.resize(first_range_size * medoid_count);

        CUCH(cudaMemcpy(result.data(), cuResult, sizeof(float) * first_range_size * medoid_count, cudaMemcpyDeviceToHost));

        if (sqrt) {
            auto sqrt_f = [](float n){ return (n > 0.000001) ? (float)std::sqrt(n) : 0.0f; };
            std::for_each(result.begin(), result.end(), sqrt_f);
        }

        return result;
    }

    void print_debug() {
        run_print_contents(cuObjectsPtr, cuObjectWeightsPtr, cuObjectCentroidCount, cuCentroidOffsets, objectCount);
        cudaDeviceSynchronize();
    }

    static inline void normalizeW(const NUM_TYPE *weights, std::size_t count, NUM_TYPE* cuPtr) {
        float res = 0;
        IN_TYPE normalizer = (IN_TYPE)0;
        for (std::size_t i = 0; i < count; ++i) {
            normalizer += (IN_TYPE)weights[i];
        }
        for (std::size_t i = 0; i < count; ++i) {
            res = weights[i] / normalizer;
            CUCH(cudaMemcpy(cuPtr, &res, sizeof(NUM_TYPE), cudaMemcpyHostToDevice));
            cuPtr += 1;
        }
    }

    void load_objects() {
        std::size_t centroidCount, centroid_processed = 0;

        NUM_TYPE *cuObjectsPtrTemp = cuObjectsPtr;
        NUM_TYPE *cuObjectWeightsPtrTemp = cuObjectWeightsPtr;
        std::size_t *cuObjectCentroidCountTemp = cuObjectCentroidCount;
        std::size_t *cuCentroidOffsetsTemp = cuCentroidOffsets;

        for (std::size_t o = 0; o < objectCount; o++) {
            centroidCount = source[o].getCentroidCount();

            cuda_memcpy_device<DIM, NUM_TYPE>(cuObjectsPtrTemp, source[o].getCoordinates(), centroidCount);
            cuda_memcpy_device<1, std::size_t>(cuObjectCentroidCountTemp++, &centroidCount, 1);
            cuda_memcpy_device<1, std::size_t>(cuCentroidOffsetsTemp++, &centroid_processed, 1);

            normalizeW(source[o].getWeights(), centroidCount, cuObjectWeightsPtrTemp);

            cuObjectsPtrTemp += DIM * centroidCount;
            cuObjectWeightsPtrTemp += centroidCount;
            centroid_processed += centroidCount;
        }
    }

    // CUDA STUFF

    void cuda_init() {
        RANKS
        CUCH(cudaSetDevice(0));
    }

    void cuda_alloc() {
        CUCH(cudaMalloc((void**)&cuObjectsPtr, sizeof(NUM_TYPE) * DIM * centroids));
        CUCH(cudaMalloc((void**)&cuResult, sizeof(float)));
        CUCH(cudaMalloc((void**)&cuObjectWeightsPtr, sizeof(NUM_TYPE) * centroids));
        CUCH(cudaMalloc((void**)&cuCentroidOffsets, sizeof(NUM_TYPE) * centroids));
        CUCH(cudaMalloc((void**)&cuObjectCentroidCount, sizeof(std::size_t) * objectCount));
        CUCH(cudaMalloc((void**)&cuAlpha, sizeof(float)));
    }

    void cuda_cluster_malloc(std::size_t cluster_size) {
        clusterSize = cluster_size;
        CUCH(cudaMalloc((void**)&cuCluster, sizeof(std::size_t) * cluster_size));
    }

    template<int D, typename NT>
    void cuda_memcpy_device(NT* dst, const NT* src, std::size_t count) {
        CUCH(cudaMemcpy((void*)dst, (void*)src, sizeof(NT) * D * count, cudaMemcpyHostToDevice));
    }

    void cuda_free() {
        CUCH(cudaFree(cuObjectsPtr));
        CUCH(cudaFree(cuObjectWeightsPtr));
        CUCH(cudaFree(cuResult));
        CUCH(cudaFree(cuObjectCentroidCount));
    }

    IN_TYPE mAlpha;

    NUM_TYPE *cuObjectsPtr;
    NUM_TYPE *cuObjectWeightsPtr;
    std::size_t *cuObjectCentroidCount;
    std::size_t *cuCentroidOffsets;
    float* cuResult, *cuResultMoving;
    std::size_t cuResultSize;
    std::size_t objectCount;

    std::size_t* cuCluster;
    std::size_t clusterSize;

    const DBSignatureListMapped<DIM, NUM_TYPE>& source;

    float* cuAlpha;
};

#endif //CUDA_DIST_HPP
