#ifndef RANKS
#define RANKS int rank, ranks; MPI_Comm_size(MPI_COMM_WORLD, &ranks); MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#endif //RANKS

#ifndef DEBUG
#define DEBUG 1
#endif //DEBUG
